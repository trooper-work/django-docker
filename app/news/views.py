from django.shortcuts import render, get_object_or_404
from .models import News
from django.views.generic import ListView
from taggit.models import Tag
from django.http import HttpResponse
from django.views.decorators.http import require_GET


class TagIndexView(ListView):
    model = News
    tempate_name = "base.html"
    context_object_name = "news"

    def get_queryset(self):
        return News.objects.filter(tags__slug=self.kwargs.get('tag_slug'))


def news_list(request):
    """ Вывод всех новостей """
    return render(request, "news/news_list.html")


def new_single(request, slug):
    """Вывод полной статьи"""

    new = get_object_or_404(News, slug=slug)
    context = {
        "new": new,
    }
    return render(request, "news/new_single.html", context)


def base(request):
    return render(request, "news/base.html")


def contacts(request):
    return render(request, "news/contacts.html")


@require_GET
def robots_txt(request):
    lines = [
        "User-agent: /*",
        "Disallow: /admin/",
        "Sitemap: https://drunken-pandaren.ru/sitemap.xml"
    ]
    return HttpResponse("\n".join(lines), content_type="text/plain")
