const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: 'development',
    entry: './static/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, './staticfiles'),
    },
    module: {
        rules: [
            {
                test: /\.(css|sass|scss)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },

                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: false,
                        }
                    },

                    {
                        loader: 'sass-loader',
                    },
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: ['file-loader'],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),

        new MiniCssExtractPlugin({
            filename: './[name].css',
            chunkFilename: '[name]_[id].css',
        }),
        new CopyPlugin({
          patterns: [
            { from: "./staticfiles/main.css", to: "/var/www/drunken-pandaren.ru/app/news/static" },
            { from: "./staticfiles/main.js", to: "/var/www/drunken-pandaren.ru/app/news/static" },
          ],
        }),
    ],
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
}
