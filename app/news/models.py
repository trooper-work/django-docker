from django.db import models
from django.contrib.auth import get_user_model
from hello_django.models import BaseNews
from filer.fields.image import FilerImageField
from django.urls import reverse

User = get_user_model()


class Category(models.Model):
    """ Класс категорий статей """

    title = models.CharField("Категория", max_length=50)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.title


class News(BaseNews):
    """ Класс новостей """

    user = models.ForeignKey(
        User,
        verbose_name="Автор",
        on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category,
        verbose_name="Категория",
        on_delete=models.SET_NULL,
        null=True)
    photo = FilerImageField(
        null=True, blank=True,
        related_name="photo_news_company",
        on_delete=models.CASCADE)

    objects = models.Manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('new_single', kwargs={'slug': self.slug})
