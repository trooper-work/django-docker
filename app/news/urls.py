from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from .views import robots_txt

from django.contrib.sitemaps.views import sitemap

from news.sitemaps import Static_Sitemap
from news.sitemaps import News_Sitemap
from news.sitemaps import WorkNews_Sitemap

sitemaps = {
    'static': Static_Sitemap(),
    'news-sitemap': News_Sitemap(),
    'worknews': WorkNews_Sitemap(),
}


urlpatterns = [
    path('base/', views.base, name="base"),
    path('', views.news_list, name="list_news"),
    path('blog/', include('works.urls')),
    path('news/<slug>', views.new_single, {}, name="new_single"),
    path('contacts/', views.contacts, name="contacts"),
    path('tags/<slug:tag_slug>/', views.TagIndexView.as_view(), name='news_by_tag'),
    path("robots.txt", robots_txt),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap')
]
