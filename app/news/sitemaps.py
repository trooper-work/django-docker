from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from .models import News
from works.models import WorkNews


class Static_Sitemap(Sitemap):

    priority = 1.0
    changefreq = 'yearly'

    def items(self):
        return ['contacts']

    def location(self, item):
        return reverse(item)


class News_Sitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def items(self):
        return News.objects.all()

    def location(self, item):
        return item.get_absolute_url()

    def lastmod(self, obj):
        return obj.created


class WorkNews_Sitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def items(self):
        return WorkNews.objects.all()

    def location(self, item):
        return item.get_absolute_url()

    def lastmod(self, obj):
        return obj.created
