from works.models import WorkNews
from news.models import News


def extras(request):

    works_news = WorkNews.objects.all()
    news = News.objects.all()

    return {"works_news": works_news,
            "news": news}
