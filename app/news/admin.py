from django.contrib import admin
from news.models import News, Category
from works.models import WorkCategory, WorkNews
from django_summernote.admin import SummernoteModelAdmin
from filer.admin import FolderAdmin
from filer.models import Folder


class MyFolderAdmin(FolderAdmin):
    owner_search_fields = ['field1', 'field2']


# Apply summernote to all TextField in model.
class SomeModelAdmin(SummernoteModelAdmin):  # instead of ModelAdmin
    summernote_fields = '__all__'


admin.site.register(News, SomeModelAdmin)
admin.site.register(WorkCategory)
admin.site.register(WorkNews, SomeModelAdmin)
admin.site.register(Category)
admin.site.unregister(Folder)
admin.site.register(Folder, MyFolderAdmin)
