from django.db import models
from django.contrib.sites.models import Site
from taggit.managers import TaggableManager
from django.utils.timezone import get_current_timezone
from datetime import datetime


class BaseNews(models.Model):
    title = models.CharField("Заголовок", max_length=100)
    text_min = models.TextField("Кор. описание", max_length=600)
    text = models.TextField("Текст статьи")
    sites = models.ManyToManyField(Site)
    tags = TaggableManager()
    created = models.DateTimeField(editable=False, auto_now_add=True)
    updated = models.DateTimeField(editable=False, auto_now=True)
    description = models.CharField("Дискрипшн", max_length=100)
    slug = models.SlugField(max_length=70, unique=True)

    class Meta:
        abstract = True
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"

    def save(self, *args, **kwargs):
        self.updated = datetime.now().replace(tzinfo=get_current_timezone())
        super(BaseNews, self).save(*args, **kwargs)
