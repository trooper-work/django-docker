from django.db import models
from django.contrib.auth import get_user_model
from hello_django.models import BaseNews
from filer.fields.image import FilerImageField
from django.urls import reverse

User = get_user_model()


class WorkCategory(models.Model):
    """ Класс категорий статей """
    title = models.CharField("Категория работ", max_length=50)

    class Meta:
        verbose_name = "Категория работ"
        verbose_name_plural = "Категории работ"

    def __str__(self):
        return self.title


class WorkNews(BaseNews):
    """ Класс новостей """

    user = models.ForeignKey(
        User,
        verbose_name="Автор",
        on_delete=models.CASCADE)
    category = models.ForeignKey(
        WorkCategory,
        verbose_name="Категория",
        on_delete=models.SET_NULL,
        null=True)
    photo = FilerImageField(
        null=True, blank=True,
        related_name="photo_works_company",
        on_delete=models.CASCADE)

    objects = models.Manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('work_news_detail', kwargs={'slug': self.slug})
