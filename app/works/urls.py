from django.urls import path
from . import views

urlpatterns = [
    path('', views.works_news_list, name="works_news_list"),
    path('article/<slug>/', views.work_news_detail, {}, name='work_news_detail'),
    path('tags/<slug:tag_slug>/', views.WorksTagIndexView.as_view(), name='works_by_tag'),
]
