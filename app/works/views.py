from django.shortcuts import render, get_object_or_404
from works.models import WorkNews
from django.views.generic import ListView
from taggit.models import Tag


class WorksTagIndexView(ListView):
    model = WorkNews
    template_name = 'works/works_news_list.html'
    context_object_name = 'works_news'

    def get_queryset(self):
        return WorkNews.objects.filter(tags__slug=self.kwargs.get('tag_slug'))


def works_news_list(request):
    works_news = WorkNews.objects.all()
    context = {
        "works_news": works_news,
    }
    return render(request, 'works/works_news_list.html', context)


def work_news_detail(request, slug):
    work_news = get_object_or_404(WorkNews, slug=slug)
    context = {
        'work_news': work_news,
    }
    return render(request, 'works/work_news_detail.html', context)


def works_base(request):
    return render(request, "works/works_base.html")
